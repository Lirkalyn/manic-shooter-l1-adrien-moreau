# on importe ce dont on a besoin
import pygame
import math
import random
import time

FPS = 60 # on limite le nombre d'images pas seconde
pygame.init() # on initialise
# on créait tous les groupes
all_sprites = pygame.sprite.Group()
bullets = pygame.sprite.Group()
enem_bullets = pygame.sprite.Group()
player_bullets = pygame.sprite.Group()
mobs = pygame.sprite.Group()
hit_box = pygame.sprite.Group()
# Création de la classe du jeu
class Manic_Shooter:
    def __init__(self):
        self._window = pygame.display.set_mode((853, 480), pygame.DOUBLEBUF) # taille de la fenêtre
        pygame.display.set_caption('Manic Shooter Adrien Moreau')
        self._bg_color = pygame.image.load("F:/!Cours/project/bg2.jpg").convert()
        self._surface = pygame.Surface((853, 480), pygame.SRCALPHA, 32)
        self._surface.fill(pygame.Color(0, 0, 250, 255))
        self._surface.blit(self._bg_color, (0, 0))
        self.clock = pygame.time.Clock() # on utilisera une horloge pour certains événements

    def run(self):
        done = False # pour arrêter la boucle principale
        free = None # free permets de bloquer le changement de pos
        pos = [400, 350] # on initialise pos
        i = 1 # i permets de conter le temps
        Start = False # Start permet d'éviter d'afficher des tirs ou le début d'un niveau avant que le vaisseau ne soit affiché
        niveau = 0 # niveau permet de lancée le bon niveau
        score = 0 # score du joueur
        # on change les noms des classes pour plus de facilité
        joueur = Player(pos)
        joueur_hit_box = Player_hit_box(pos)
        acolyte = Acolyte()
        acolyte2 = Acolyte2()
        acolyte3 = Acolyte3()
        acolyte4 = Acolyte4()
        # début de la boucle principale
        while not done:
            self.clock.tick(FPS) # on utilise l'horloge pour limiter la vitesse de la boucle
            for event in pygame.event.get():
                self._window.blit(self._bg_color, (0, 0))
                if event.type == pygame.QUIT:
                    done = True
                elif event.type == pygame.KEYDOWN: # ça permet de choisir le niveau en appuyant sur les touches du clavier
                    if (event.key == pygame.K_1):
                        niveau = 1
                    elif (event.key == pygame.K_2):
                        niveau = 2
                    elif (event.key == pygame.K_3):
                        niveau = 3
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    button = event.button
                    if button == 1: # Permets de créer une et une seule instance des classes ci-dessous et aussi de les rajouter dans un groupe
                        free = 1
                        Acolyte().update(pos, i)
                        Acolyte2().update(pos, i)
                        Acolyte3().update(pos, i)
                        Acolyte4().update(pos, i)
                        all_sprites.add(joueur)
                        all_sprites.add(joueur_hit_box)
                        hit_box.add(joueur_hit_box)
                        all_sprites.add(acolyte)
                        all_sprites.add(acolyte2)
                        all_sprites.add(acolyte3)
                        all_sprites.add(acolyte4)
                        Start = True # autorise les tir
                elif free != None:
                    if event.type == pygame.MOUSEMOTION: # Gère les mouvements du joueur et des acolytes en les limitant
                        pos[0], pos[1] = event.pos[0], event.pos[1]
                        if event.pos[0] < 284:
                            pos[0] = 284
                            if event.pos[1] <= 1:
                                pos[0] = 284
                                pos[1] = 1
                            elif event.pos[1] > 479:
                                pos[0] = 284
                                pos[1] = 479
                        elif event.pos[0] > 568:
                            pos[0] = 568
                            if event.pos[1] <= 1:
                                pos[0] = 568
                                pos[1] = 1
                            elif event.pos[1] > 479:
                                pos[0] = 568
                                pos[1] = 479
                        else:
                            pos[0] = event.pos[0]
                            pos[1] = event.pos[1]
                    #Player(pos).update(pos, i)
            # permets de tirer en continue sans utiliser de touche
            joueur.tir()
            acolyte.tir(Start)
            acolyte2.tir(Start)
            acolyte3.tir(Start)
            acolyte4.tir(Start)
            i += 1 # Permets de conter le temps et de l'envoyer là où on a besoin de lui
            # lance le niveau souhaité
            if niveau == 1:
                Niv().niv_1(i, Start)
            elif niveau == 2:
                Niv().niv_2(i, Start)
            elif niveau == 3:
                Niv().niv_3(i, Start)
            # Gère les collisions entre les ennemies et les balles du joueur / acolyte + augmente le score quand un ennemi est détruit
            hits = pygame.sprite.groupcollide(mobs, player_bullets, False, True)
            for g in hits:
                g.pv -= 10
                if g.pv <= 0:
                    g.kill()
                    score += 250
            hits_2 = pygame.sprite.groupcollide(hit_box, enem_bullets, False, True)
            for h in hits_2:
                h.pv -= 1
                if h.pv <= 0:
                    time.sleep(30) # offre un délai de 30 secondes avant la fermeture pour pouvoir prendre son score en photo / screenshoot si voulue
                    done = True # ferme le jeux
            polices = pygame.font.SysFont("Times New Roman", 18) # choix de la police et de sa taille d'écriture pour le score
            Score = polices.render("Votre score est : " + str(score), 1, (255,255,255)) # stockage du texte a écrire dans Score
            all_sprites.update(pos, i) # bouge tout les sprites
            self._window.blit(self._bg_color, (0, 0)) 
            self._window.blit(Score, (0, 0)) # affiche le score a l'écran
            all_sprites.draw(self._window)
            pygame.display.flip()

# cette classe gère le joueur, sa position et ces tirs
class Player(pygame.sprite.Sprite):
    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/perfect5.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.centerx = 0
        self.rect.bottom = 0
        self.pos = pos

    def update(self, pos, i):
        self.rect.centerx, self.rect.centery = pos[0], pos[1] - 40

    def tir(self):
        tir = Balle((self.rect.centerx, self.rect.centery))
        all_sprites.add(tir)
        player_bullets.add(tir)

# cette classe gère la hit bos du joueur, sa position et sa vie
class Player_hit_box(pygame.sprite.Sprite):
    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((5, 5))
        self.image.fill((255, 255, 0))
        self.rect = self.image.get_rect()
        self.rect.centerx = 0
        self.rect.bottom = 0
        self.pos = pos
        self.pv = 3

    def update(self, pos, i):
        self.rect.centerx, self.rect.centery = pos[0], pos[1]

# cette classe gère les balles du joueur, leurs positions, vitesse et disparition
class Balle(pygame.sprite.Sprite):
    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/bullet_lv_3.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.centerx = pos[0]
        self.rect.bottom = pos[1]
        self.speedy = -8

    def update(self, pos, i):
        self.rect.y += self.speedy
        if self.rect.bottom < 0:
            self.kill()

# cette classe gère les balles des acolytes, leurs positions, vitesse et disparition
class Balle_Acolyte(pygame.sprite.Sprite):
    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/Acolyte_mun3 - Copie (2).png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.centerx = pos[0]
        self.rect.bottom = pos[1]
        self.speedy = -8

    def update(self, pos, i):
        self.rect.y += self.speedy
        if self.rect.bottom < 0:
            self.kill()

# cette classe gère les balles des ennemies, leurs positions, vitesse et disparition
class Balle_Enemy(pygame.sprite.Sprite):
    def __init__(self, pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/ene_bull.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.centerx = pos[0]
        self.rect.bottom = pos[1]
        self.speedy = -8

    def update(self, pos, i):
        self.rect.y -= self.speedy
        if self.rect.bottom < 0:
            self.kill()

# cette classe gère un type d'ennemies, sa positions, vitesse et disparition. elle gère aussi la vitesse de tir de l'enemie
class Ennemie(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/tempo_ene3.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.x = 300
        self.rect.y = 0
        self.speedy = 2 # vitesse de l'ennemies
        self.pv = 600 # vie de l'ennemies

    def update(self, pos, i):
        self.rect.y += self.speedy
        self.rect.x += self.speedy
        if self.rect.bottom > 479:
            self.kill()
        elif self.rect.x > 568:
            self.kill()
        elif self.rect.x < 284:
            self.kill()
        if i % (FPS / 1.25) == 0: # ici on s'occupe de la vitesse de feu
            tir = Balle_Enemy((self.rect.centerx, (self.rect.centery + 50)))
            all_sprites.add(tir)
            enem_bullets.add(tir)
            i += 1 # permet de sortir de la boucle

# identique seule la direction change
class Ennemie_2(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/tempo_ene3.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.x = 478
        self.rect.y = 0
        self.speedy = 2 # vitesse de l'ennemies
        self.pv = 600 # vie de l'ennemies

    def update(self, pos, i):
        self.rect.y += self.speedy
        self.rect.x -= self.speedy
        if self.rect.bottom > 479:
            self.kill()
        elif self.rect.x > 568:
            self.kill()
        elif self.rect.x < 284:
            self.kill()
        if i % (FPS / 1.25) == 0:
            tir = Balle_Enemy((self.rect.centerx, (self.rect.centery + 50)))
            all_sprites.add(tir)
            enem_bullets.add(tir)
            i += 1 # permet de sortir de la boucle

# identique seule la direction change
class Ennemie_3(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/tempo_ene3.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.x = random.randrange(300, 550) # Apparition au hasard de l'ennemie sur cet axe
        self.rect.y = 0
        self.speedy = 2 # vitesse de l'ennemies
        self.pv = 600 # vie de l'ennemies

    def update(self, pos, i):
        self.rect.y += self.speedy
        if self.rect.bottom > 479:
            self.kill()
        elif self.rect.x > 568:
            self.kill()
        elif self.rect.x < 284:
            self.kill()
        if i % (FPS / 1.25) == 0:
            tir = Balle_Enemy((self.rect.centerx, (self.rect.centery + 50)))
            all_sprites.add(tir)
            enem_bullets.add(tir)
            i += 1 # permet de sortir de la boucle

# cette classe gère le boss ennemies, sa positions, vitesse et disparition. elle gère aussi la vitesse de tir de l'enemie
class Boss(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/boss.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rect.x = random.randrange(300, 550) # Apparition au hasard de l'ennemie sur cet axe
        self.rect.y = 0
        self.speedy = 3
        self.bord = None # Permets de faire rebondir l'ennemie sur les bords pour qu'il bouge sans pour autant être hors d'atteinte
        self.pv = 1200 # vie du boss 

    def update(self, pos, i): 
        # Permets de gérer le va et viens du boss
        if self.rect.x > 568:
            self.bord = 1
        elif self.rect.x < 284:
            self.bord = 2
        if self.bord == 1:
            self.rect.x -= self.speedy
        elif self.bord == 2:
            self.rect.x += self.speedy
        elif self.bord == None:
            self.rect.x += self.speedy
        if i % (FPS / 3.75) == 0: # comme c'est le boss il tire plus vite et depuis plus d'endroit
            tir = Balle_Enemy((self.rect.centerx, (self.rect.centery + 75)))
            tir_2 = Balle_Enemy((self.rect.centerx, (self.rect.centery + 15)))
            tir_3 = Balle_Enemy((self.rect.centerx, (self.rect.centery - 45)))
            all_sprites.add(tir)
            all_sprites.add(tir_2)
            all_sprites.add(tir_3)
            enem_bullets.add(tir)
            enem_bullets.add(tir_2)
            enem_bullets.add(tir_3)
            i += 1 # permet de sortir de la boucle

# cette classe gère un des Acolyte, sa position, ces tirs et sa vitesse
class Acolyte(pygame.sprite.Sprite):
    
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/Acolyte_5.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rayons = 55 # rayon de rotation de l'acolyte autour du curseur
        self.angle = 0 # angle de départ
        self.vitesse = 3 # vitesse de rotation

    def update(self, pos, i):
        self.angle += self.vitesse
        # cette formule gère la rotation continue de l'acolyte grâce aux données ci-dessus
        self.rect.centerx = pos[0] + self.rayons * math.cos(math.radians(self.angle))
        self.rect.centery = (pos[1] - 40) + self.rayons * math.sin(math.radians(self.angle))

    def tir(self, Start):
        if Start == True: # Permets de tirer lorsque le vaisseau du joueur est apparue
            tir = Balle_Acolyte((self.rect.centerx, (self.rect.centery - 5)))
            all_sprites.add(tir)
            player_bullets.add(tir)

# identique seule la position de départ change
class Acolyte2(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/Acolyte_5.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rayons = 55
        self.angle = 90
        self.vitesse = 3

    def update(self, pos, i):
        self.angle += self.vitesse
        self.rect.centerx = pos[0] + self.rayons * math.cos(math.radians(self.angle))
        self.rect.centery = (pos[1] - 40) + self.rayons * math.sin(math.radians(self.angle))

    def tir(self, Start):
        if Start == True:
            tir = Balle_Acolyte((self.rect.centerx, (self.rect.centery - 5)))
            all_sprites.add(tir)
            player_bullets.add(tir)

# identique seule la position de départ change
class Acolyte3(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/Acolyte_5.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rayons = 55
        self.angle = 180
        self.vitesse = 3

    def update(self, pos, i):
        self.angle += self.vitesse
        self.rect.centerx = pos[0] + self.rayons * math.cos(math.radians(self.angle))
        self.rect.centery = (pos[1] - 40) + self.rayons * math.sin(math.radians(self.angle))

    def tir(self, Start):
        if Start == True:
            tir = Balle_Acolyte((self.rect.centerx, (self.rect.centery - 5)))
            all_sprites.add(tir)
            player_bullets.add(tir)

# identique seule la position de départ change
class Acolyte4(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("F:/!Cours/project/Acolyte_5.png").convert()
        self.image.set_colorkey(pygame.Color(0, 255, 0, 255))
        self.rect = self.image.get_rect()
        self.rayons = 55
        self.angle = 270
        self.vitesse = 3

    def update(self, pos, i):
        self.angle += self.vitesse
        self.rect.centerx = pos[0] + self.rayons * math.cos(math.radians(self.angle))
        self.rect.centery = (pos[1] - 40) + self.rayons * math.sin(math.radians(self.angle))

    def tir(self, Start):
        if Start == True:
            tir = Balle_Acolyte((self.rect.centerx, (self.rect.centery - 5)))
            all_sprites.add(tir)
            player_bullets.add(tir)

# cette classe gère les niveaux donc l'ordre d'apparitions, le nombre d'apparitions et tout cela axé sur le temps
class Niv:
    def __init__(self):
        # Ça nous permet d'utiliser ces noms partout dans la classe
        self.boss = Boss()
        self.ennemie = Ennemie()
        self.ennemie_2 = Ennemie_2()
        self.ennemie_3 = Ennemie_3()

    def niv_1(self, temps, Start): # récupérer le temps et l'autorisation de commencer
        if Start == True:
            if temps < (FPS * 5): # limitation de la durée pour apparaître
                while temps % (FPS/1.5) == 0: # boucle pour apparaître à une certaine fréquence
                    all_sprites.add(self.ennemie)
                    mobs.add(self.ennemie)
                    temps += 1 # permet de sortir de la boucle
            elif temps > (FPS * 5) and temps < (FPS * 10):
                while temps % (FPS/1.5) == 0:
                    all_sprites.add(self.ennemie_2)
                    mobs.add(self.ennemie_2)
                    temps += 1
            elif temps > (FPS * 10) and temps < (FPS * 15):
                while temps % (FPS/1.5) == 0:
                    all_sprites.add(self.ennemie_3)
                    mobs.add(self.ennemie_3)
                    temps += 1
            elif temps == (FPS * 16):
                all_sprites.add(self.boss)
                mobs.add(self.boss)

    def niv_2(self, temps, Start):
        if Start == True:
            if temps < (FPS * 10):
                while temps % (FPS/1.5) == 0:
                    all_sprites.add(self.ennemie_3)
                    all_sprites.add(self.ennemie)
                    mobs.add(self.ennemie_3)
                    mobs.add(self.ennemie)
                    temps += 1
            elif temps > (FPS * 10) and temps < (FPS * 25):
                while temps % (FPS/1.5) == 0:
                    all_sprites.add(self.ennemie_2)
                    all_sprites.add(self.ennemie_3)
                    mobs.add(self.ennemie_2)
                    mobs.add(self.ennemie_3)
                    temps += 1
            elif temps > (FPS * 25) and temps < (FPS * 35):
                while temps % (FPS/1.25) == 0:
                    all_sprites.add(self.ennemie_3)
                    mobs.add(self.ennemie_3)
                    temps += 1
            elif temps == (FPS * 37):
                all_sprites.add(self.boss)
                mobs.add(self.boss)

    def niv_3(self, temps, Start):
        if Start == True:
            if temps < (FPS * 15):
                while temps % (FPS/1.25) == 0:
                    all_sprites.add(self.ennemie)
                    all_sprites.add(self.ennemie_2)
                    mobs.add(self.ennemie)
                    mobs.add(self.ennemie_2)
                    temps += 1
            elif temps > (FPS * 15) and temps < (FPS * 25):
                while temps % (FPS/1) == 0:
                    all_sprites.add(self.ennemie_3)
                    mobs.add(self.ennemie_3)
                    temps += 1
            elif temps == (FPS * 27):
                all_sprites.add(self.boss)
                mobs.add(self.boss)

if __name__ == '__main__':
    mp = Manic_Shooter()
    mp.run()